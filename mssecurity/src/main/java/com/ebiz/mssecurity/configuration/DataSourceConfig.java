package com.ebiz.mssecurity.configuration;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DataSourceConfig {
	
	
	@Bean(name = "dsSession")
	@Primary
	@ConfigurationProperties(prefix="spring.datasource.db-oauth2")
	public DataSource dbPrimaryDataSource() {
	    return DataSourceBuilder.create().build();
	}

	
	@Bean
	@Primary
	public JdbcTemplate primaryJdbcTemplate( @Qualifier("dsSession") DataSource dsSession) {
	    return new JdbcTemplate(dsSession);
	}
	
	@Bean(name = "dbCore")
	@ConfigurationProperties(prefix="spring.datasource.db-core")
	public DataSource dbCoreDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean
	@Qualifier("jdbcCore")
	public JdbcTemplate dbCoreJdbcTemplate(@Qualifier("dbCore")  DataSource dbCore) {
	    return new JdbcTemplate(dbCore);
	}
	
}
