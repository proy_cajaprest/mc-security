package com.ebiz.mssecurity.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.common.security.Organizacion;
import com.ebiz.common.security.User;
import com.ebiz.mssecurity.dao.UserRepository;
import com.ebiz.common.security.Opcion;

@Repository()
public class UserRepositoryImpl implements UserRepository {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	@Qualifier("jdbcCore")
	private JdbcTemplate jdbcTemplateCore;

	private static String USUARIO_SELECT = "SELECT in_idusuario, vc_nombre, vc_apellidopaterno, vc_apellidomaterno, "
			+ "vc_direccion, ch_telefono, ch_anexo, vc_email, vc_tipodocumento, vc_numerodocumento, vc_usuario, "
			+ "vc_password, vc_campo1, vc_campo2, vc_campo3, vc_campo4, vc_campo5, in_habilitado, vc_contacto, ch_titulo, "
			+ "ch_fax, fl_montomaximo, in_habilitarenviomails, in_poraprobar, vc_idioma, in_boletin, vc_idtablamoneda, "
			+ "vc_idregistromoneda, vc_idtablapais, vc_idregistropais, vc_avatar "
			+ "FROM t_usuario WHERE vc_usuario = ? and in_habilitado = 1";

	private static String USUARIO_ORGANIZACION_SELECT = "SELECT O.in_idorganizacion, O.in_idgrupo, O.vc_moneda, "
			+ "O.vc_nombre, O.vc_direccion, O.ch_telefono, O.vc_email, O.ch_fax, O.vc_ruc, O.vc_url, O.vc_campo1, "
			+ "O.vc_campo2, O.vc_campo3, O.vc_campo4, O.vc_campo5, O.in_habilitado, O.vc_tipoempresa, O.vc_key_suscripcion, O.vc_isoPais, O.vc_logo "
			+ "FROM administracion.t_organizacion O "
			+ "INNER JOIN administracion.t_usuarioxorganizacion uo on uo.in_idorganizacion = O.in_idorganizacion "
			+ "WHERE O.in_habilitado=1 and uo.in_idusuario = ?";


	/*private static String OPCION_USUARIO_SELECT = "SELECT O.in_idopcion, O.in_idpantalla, O.vc_descripcion from administracion.t_opcion O "
			+ " inner join  administracion.t_rolxopcion R on O.in_idopcion = R.in_idopcion "
			+ " inner join administracion.t_usuarioxorganizacion U on R.in_idrol=U.in_idrol where  U.in_idusuario=?";*/
				

	@Override
	public List<Organizacion> loadOrganizacion(UUID userId) {

		try {

			return jdbcTemplateCore.query(USUARIO_ORGANIZACION_SELECT, new Object[] { userId }, this::mapOrganizacion);

		} catch (EmptyResultDataAccessException e) {
			logger.error("Organizacion not found: {}", userId, e);
			return null;
		}

	}

	public User findUserById(String name) {
		try {

			return (User) jdbcTemplateCore.queryForObject(USUARIO_SELECT, new Object[] { name }, this::mapUser);

		} catch (EmptyResultDataAccessException e) {
			logger.error("User not found: {}", name, e);
			return null;
		}

	}



	/*public List<Opcion> loadOpcion(UUID userId) {

		try {
			return jdbcTemplateCore.query(OPCION_USUARIO_SELECT, new Object[] { userId }, this::mapOpcion);

		} catch (EmptyResultDataAccessException e) {
			logger.error("Opcion not found: {}", userId, e);
			return null;
		}

	}*/

	public Organizacion mapOrganizacion(ResultSet rs, int i) throws SQLException {
		
		String codeBytes = rs.getString("in_idorganizacion");
		UUID organizacionId = UUID.fromString(codeBytes);

		Organizacion organizacion = new Organizacion();
		organizacion.setId(organizacionId);

		organizacion.setNombre(StringUtils.trimToEmpty(rs.getString("vc_nombre")));
		organizacion.setTipoEmpresa(StringUtils.trimToEmpty(rs.getString("vc_tipoempresa")));
		organizacion.setKeySuscripcion(StringUtils.trimToEmpty(rs.getString("vc_key_suscripcion")));
		organizacion.setIsoPais(StringUtils.trimToEmpty(rs.getString("vc_isoPais")));
		if(rs.getString("vc_ruc")!=null){
			//String rucResp = "PE"+rs.getString("vc_ruc");
			String rucResp = rs.getString("vc_ruc");
			organizacion.setRuc(rucResp);
		}else{
			organizacion.setRuc("");}
		organizacion.setLogo(StringUtils.trimToEmpty(rs.getString("vc_logo")));
		
		return organizacion;

	}


	/*public Opcion mapOpcion(ResultSet rs, int i) throws SQLException {

		//String codeBytes = rs.getString("in_idopcion");
		//UUID opcionId = UUID.fromString(codeBytes);

		Opcion opcion = new Opcion();
		
		opcion.setIn_idopcion(StringUtils.trimToEmpty(rs.getString("in_idopcion")));
		opcion.setIn_idpantalla(StringUtils.trimToEmpty(rs.getString("in_idpantalla")));
		opcion.setVc_descripcion(StringUtils.trimToEmpty(rs.getString("vc_descripcion")));
		
		return opcion;

	}*/

	public User mapUser(ResultSet rs, int i) throws SQLException {

		String codeBytes = rs.getString("in_idusuario");
		UUID userID = UUID.fromString(codeBytes);

		User user = new User();
		user.setId(userID);
		user.setNombre(StringUtils.trimToEmpty(rs.getString("vc_nombre")));
		user.setApellidoPaterno(StringUtils.trimToEmpty(rs.getString("vc_apellidopaterno")));
		user.setApellidoMaterno(StringUtils.trimToEmpty(rs.getString("vc_apellidomaterno")));
		user.setDireccion(StringUtils.trimToEmpty(rs.getString("vc_direccion")));
		user.setTelefono(StringUtils.trimToEmpty(rs.getString("ch_telefono")));
		user.setAnexo(StringUtils.trimToEmpty(rs.getString("ch_anexo")));
		user.setEmail(StringUtils.trimToEmpty(rs.getString("vc_email")));
		user.setTipoDocumento(StringUtils.trimToEmpty(rs.getString("vc_tipodocumento")));
		user.setNumeroDocumento(StringUtils.trimToEmpty(rs.getString("vc_numerodocumento")));
		user.setUsuario(StringUtils.trimToEmpty(rs.getString("vc_usuario")));
		user.setPassword(StringUtils.trimToEmpty(rs.getString("vc_password")));
		user.setCampo1(StringUtils.trimToEmpty(rs.getString("vc_campo1")));
		user.setCampo2(StringUtils.trimToEmpty(rs.getString("vc_campo2")));
		user.setCampo3(StringUtils.trimToEmpty(rs.getString("vc_campo3")));
		user.setCampo4(StringUtils.trimToEmpty(rs.getString("vc_campo4")));
		user.setCampo5(StringUtils.trimToEmpty(rs.getString("vc_campo5")));
		user.setHabilitado(rs.getShort("in_habilitado"));
		user.setContacto(StringUtils.trimToEmpty(rs.getString("vc_contacto")));
		user.setTitulo(StringUtils.trimToEmpty(rs.getString("ch_titulo")));
		user.setFax(StringUtils.trimToEmpty(rs.getString("ch_fax")));
		user.setMontoMaximo(rs.getDouble("fl_montomaximo"));
		user.setHabilitadoEnvioMails(rs.getShort("in_habilitarenviomails"));
		user.setPorAprobar(rs.getShort("in_poraprobar"));
		user.setIdioma(StringUtils.trimToEmpty(rs.getString("vc_idioma")));
		user.setBoletin(rs.getShort("in_boletin"));
		user.setMonedaTablaId(StringUtils.trimToEmpty(rs.getString("vc_idtablamoneda")));
		user.setMonedaRegistroId(StringUtils.trimToEmpty(rs.getString("vc_idregistromoneda")));
		user.setPaisTablaId(StringUtils.trimToEmpty(rs.getString("vc_idtablapais")));
		user.setPaisRegistroId(StringUtils.trimToEmpty(rs.getString("vc_idregistropais")));
		user.setAvatar(StringUtils.trimToEmpty(rs.getString("vc_avatar")));

		return user;
	}

}
