package com.ebiz.mssecurity.dao;

import java.util.List;
import java.util.UUID;

import com.ebiz.common.security.Organizacion;
import com.ebiz.common.security.User;
import com.ebiz.common.security.Opcion;

public interface UserRepository {
	User findUserById(String name);
	List<Organizacion> loadOrganizacion(UUID userId);
	//List<Opcion> loadOpcion(UUID userId);
	
}
