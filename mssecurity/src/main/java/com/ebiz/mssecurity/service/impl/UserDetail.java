package com.ebiz.mssecurity.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.ebiz.common.security.User;

public class UserDetail extends org.springframework.security.core.userdetails.User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final boolean accountNonExpired = true;
	private static final boolean credentialsNonExpired = true;
	
	private final User user;

	/*
	 * private final String mail; private final String tipoUsuario; private final
	 * String tipoRuta;
	 */

	public UserDetail(User usuario) {
		
		super(usuario.getUsuario(), usuario.getPassword(),
				true, accountNonExpired, credentialsNonExpired, usuario.getHabilitado()==1, getGrantedAuthorities(usuario));
		
		this.user = usuario;
		this.user.setPassword(StringUtils.EMPTY);
		
		/*
		 * this.mail=usuario.getMail(); this.tipoUsuario=usuario.getTipoUsuario();
		 * this.tipoRuta=usuario.getTipoRuta();
		 */

	}
	

	private static List<GrantedAuthority> getGrantedAuthorities(User user) {
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("USER"));
		return authorities;
	}


	public User getUser() {
		return user;
	}



}
