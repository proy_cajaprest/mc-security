package com.ebiz.mssecurity.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ebiz.common.security.Organizacion;
import com.ebiz.common.security.User;
import com.ebiz.mssecurity.dao.UserRepository;
import com.ebiz.common.security.Opcion;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserRepository userRepository;
	
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.findUserById(username);

		if (user == null) {
			logger.info("User not found: {}",username);
			throw new UsernameNotFoundException("Username not found");
		}
		
		List<Organizacion> list = userRepository.loadOrganizacion(user.getId());
		if(null!=list) {
			user.getOrganizaciones().addAll(list);
		}
		
		
		/*List<Opcion> listOpcion = userRepository.loadOpcion(user.getId());
		if(null!=listOpcion) {
			user.getOpciones().addAll(listOpcion);
		}*/		

		
		return new UserDetail(user);

	}
	

}
