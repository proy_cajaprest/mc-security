#
FROM frolvlad/alpine-oraclejdk8:slim
ARG PORT_EXPOSE=8082
ADD ./app.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE ${PORT_EXPOSE}
ENV JAVA_OPTS=""
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]